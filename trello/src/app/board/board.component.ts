import { BoardService } from './../board.service';
import { Component, OnInit } from '@angular/core';
// import { BoardService } from 'board.BoardService'

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  boardName: Array<string> = [];
  boardId: Array<string> = [];
  createBoardName: any = '';

  constructor(public boardService: BoardService) {
    boardService.getBoard().then(data => JSON.parse(JSON.stringify(data)))
      .then(data => {
        for (let board of data) {
          this.boardName.push(board.name)
          this.boardId.push(board.id)
        }
      })
  }

  createBoard() {
    this.boardName.push(this.createBoardName)
    this.boardService.postBoard(this.createBoardName)

  };
  boardNamecreate(event: any) {
    // console.log(event.target.value)
    this.createBoardName = (<HTMLInputElement>event.target).value;
  }

  ngOnInit(): void {
  }

}
