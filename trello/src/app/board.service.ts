import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor(private http: HttpClient) {
  }


  getBoard = () => {
    return new Promise((resolve, reject) => {
      //Get Boards that Member belongs to
      this.http.get(`https://api.trello.com/1/members/me/boards?key=${environment.key}&token=${environment.token}`)
        .subscribe((res => {
          resolve(res)
        }))
    })
  }

  postBoard = (name: any) => {
    return new Promise((resolve, reject) => {

      //
      const headers = { 'content-type': 'application/json' }
      this.http.post(`https://api.trello.com/1/boards/?key=${environment.key}&token=${environment.token}&name=${name}`, headers)
        .subscribe((res) => {
          resolve(res)
        })
    })
  }
}
